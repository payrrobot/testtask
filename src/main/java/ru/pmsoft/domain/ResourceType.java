package ru.pmsoft.domain;

public enum ResourceType {
  LABOR, MATERIAL, EQUIPMENT
}
