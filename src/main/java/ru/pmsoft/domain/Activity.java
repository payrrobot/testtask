package ru.pmsoft.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Activity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  @Column(nullable = false)
  private String code;
  @Column(nullable = false)
  private String name;
  @Column(name = "planned_start_date")
  private Date plannedStartDate;
  @Column(name = "planned_finish_date")
  private Date plannedFinishDate;
  @Column(name = "actual_start_date")
  private Date actualStartDate;
  @Column(name = "actual_finish_date")
  private Date actualFinishDate;
  @JsonIgnore
  @ManyToOne
  private Project project;
  @OneToMany(mappedBy = "activity", orphanRemoval = true, cascade = {
      CascadeType.ALL}, fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  private List<ResourceAssignment> resourceAssignments;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getPlannedStartDate() {
    return plannedStartDate;
  }

  public void setPlannedStartDate(Date plannedStartDate) {
    this.plannedStartDate = plannedStartDate;
  }

  public Date getPlannedFinishDate() {
    return plannedFinishDate;
  }

  public void setPlannedFinishDate(Date plannedFinishDate) {
    this.plannedFinishDate = plannedFinishDate;
  }

  public Date getActualStartDate() {
    return actualStartDate;
  }

  public void setActualStartDate(Date actualStartDate) {
    this.actualStartDate = actualStartDate;
  }

  public Date getActualFinishDate() {
    return actualFinishDate;
  }

  public void setActualFinishDate(Date actualFinishDate) {
    this.actualFinishDate = actualFinishDate;
  }

  public List<ResourceAssignment> getResourceAssignments() {
    return resourceAssignments;
  }

  public void setResourceAssignments(List<ResourceAssignment> resourceAssignments) {
    this.resourceAssignments = resourceAssignments;
    if (resourceAssignments != null) {
      resourceAssignments.forEach(assignment -> assignment.setActivity(this));
    }
  }
}
