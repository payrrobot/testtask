package ru.pmsoft.domain.controller;

import org.springframework.web.bind.annotation.*;
import ru.pmsoft.domain.Activity;
import ru.pmsoft.domain.dto.ProjectDto;
import ru.pmsoft.domain.service.DefaultProjectService;
import ru.pmsoft.domain.service.ProjectService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/projects")
public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(DefaultProjectService projectService) {
        this.projectService = projectService;
    }
    //TODO добавить логи

    @PostMapping
    public ProjectDto saveProject(@RequestBody ProjectDto projectDto) {
        projectDto.getActivities()
                .stream()
                .peek(activity -> activity.setActualFinishDate(new Date(System.currentTimeMillis())))
                .peek(activity -> activity.setActualStartDate(new Date(System.currentTimeMillis())))
                .collect(Collectors.toList());
        projectDto.getActivities()
                .stream()
                .peek(activity -> activity
                        .getResourceAssignments()
                        .stream()
                        .peek(resourceAssignment -> resourceAssignment
                                .setActualUnits(BigDecimal.valueOf(Math.random()*20)))
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        return projectService.saveProject(projectDto);
    }

    @GetMapping("/{projectId}")
    public ProjectDto findById(@PathVariable Integer projectId) {
        return projectService.findById(projectId);
    }

    @GetMapping
    public List<ProjectDto> findAll() {
        return projectService.findAll();
    }

    @DeleteMapping("/{projectId}")
    public void deleteProject(@PathVariable Integer projectId) {
        projectService.deleteProject(projectId);
    }
}
