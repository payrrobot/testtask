package ru.pmsoft.domain.dto;

import java.math.BigDecimal;

public class ResourceAssigmentDto {

    private int id;
    private int resourceId;
    private BigDecimal plannedUnits;
    private BigDecimal actualUnits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public BigDecimal getPlannedUnits() {
        return plannedUnits;
    }

    public void setPlannedUnits(BigDecimal plannedUnits) {
        this.plannedUnits = plannedUnits;
    }

    public BigDecimal getActualUnits() {
        return actualUnits;
    }

    public void setActualUnits(BigDecimal actualUnits) {
        this.actualUnits = actualUnits;
    }
}
