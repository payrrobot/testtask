package ru.pmsoft.domain.dto;

import ru.pmsoft.domain.Project;
import ru.pmsoft.domain.ResourceAssignment;

import java.util.Date;
import java.util.List;

public class ActivityDto {
    private int id;
    private String code;
    private String name;
    private Date plannedStartDate;
    private Date plannedFinishDate;
    private Date actualStartDate;
    private Date actualFinishDate;
    private List<ResourceAssignment> resourceAssignments;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public Date getPlannedFinishDate() {
        return plannedFinishDate;
    }

    public void setPlannedFinishDate(Date plannedFinishDate) {
        this.plannedFinishDate = plannedFinishDate;
    }

    public Date getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(Date actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public void setActualStartDate(String actualStartDate) {
        this.actualStartDate = new Date(Long.parseLong(actualStartDate));
    }

    public Date getActualFinishDate() {
        return actualFinishDate;
    }

    public void setActualFinishDate(Date actualFinishDate) {
        this.actualFinishDate = actualFinishDate;
    }

    public void setActualFinishDate(String actualFinishDate) {
        this.actualFinishDate = new Date(Long.parseLong(actualFinishDate));
    }

    public List<ResourceAssignment> getResourceAssignments() {
        return resourceAssignments;
    }

    public void setResourceAssignments(List<ResourceAssignment> resourceAssignments) {
        this.resourceAssignments = resourceAssignments;
    }
}
