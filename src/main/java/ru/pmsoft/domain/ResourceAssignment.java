package ru.pmsoft.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="resource_assignment")
public class ResourceAssignment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  @Column(name = "resource_id", nullable = false)
  private int resourceId;
  @Column(name = "planned_units")
  private BigDecimal plannedUnits;
  @Column(name = "actual_units")
  private BigDecimal actualUnits;
  @JsonIgnore
  @ManyToOne
  private Activity activity;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public int getResourceId() {
    return resourceId;
  }

  public void setResourceId(int resourceId) {
    this.resourceId = resourceId;
  }

  public BigDecimal getPlannedUnits() {
    return plannedUnits;
  }

  public void setPlannedUnits(BigDecimal plannedUnits) {
    this.plannedUnits = plannedUnits;
  }

  public BigDecimal getActualUnits() {
    return actualUnits;
  }

  public void setActualUnits(BigDecimal actualUnits) {
    this.actualUnits = actualUnits;
  }
}
