package ru.pmsoft.domain.service;

import org.springframework.stereotype.Component;
import ru.pmsoft.domain.Project;
import ru.pmsoft.domain.dto.ProjectDto;

@Component
public class ProjectConverter {

    public Project fromProjectDtoToProject(ProjectDto projectDTO) {
        Project project = new Project();
        project.setId(projectDTO.getId());
        project.setCode(projectDTO.getCode());
        project.setName(projectDTO.getName());
        project.setActivities(projectDTO.getActivities());
        return project;
    }

    public ProjectDto fromProjectToProjectDto(Project project) {
        ProjectDto projectDTO = new ProjectDto();
        projectDTO.setId(project.getId());
        projectDTO.setCode(project.getCode());
        projectDTO.setName(project.getName());
        projectDTO.setActivities(project.getActivities());
        return projectDTO;
    }
}
