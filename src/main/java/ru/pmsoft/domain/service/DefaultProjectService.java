package ru.pmsoft.domain.service;

import org.springframework.stereotype.Service;
import ru.pmsoft.domain.Project;
import ru.pmsoft.domain.dto.ProjectDto;
import ru.pmsoft.domain.repository.ProjectRepository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultProjectService implements ProjectService{


    private final ProjectRepository projectRepository;
    private final ProjectConverter projectConverter;

    public DefaultProjectService(ProjectRepository projectRepository, ProjectConverter projectConverter) {
        this.projectRepository = projectRepository;
        this.projectConverter = projectConverter;
    }

    public ProjectDto saveProject(ProjectDto projectDto) {
        //тут должн быть вызов метода валидации
        Project savedProject = projectRepository.save(projectConverter.fromProjectDtoToProject(projectDto));
        return projectConverter.fromProjectToProjectDto(savedProject);
    }

    public void deleteProject(Integer projectId) {
        projectRepository.deleteById(projectId);
    }

    public ProjectDto findById(Integer projectId) {
        ProjectDto projectDto = projectConverter.fromProjectToProjectDto(projectRepository.findById(projectId).get());
        return projectDto;
    }

    public List<ProjectDto> findAll() {
        return projectRepository.findAll()
                .stream()
                .map(projectConverter::fromProjectToProjectDto)
                .collect(Collectors.toList());
    }

    public Date nowDate() {
        return new Date(System.currentTimeMillis());
    }
}
