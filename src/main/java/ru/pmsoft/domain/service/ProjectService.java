package ru.pmsoft.domain.service;

import ru.pmsoft.domain.dto.ProjectDto;

import java.util.Date;
import java.util.List;

public interface ProjectService {

    ProjectDto saveProject(ProjectDto projectDTO);

    ProjectDto findById(Integer projectId);

    void deleteProject(Integer projectId);

    List<ProjectDto> findAll();

}
