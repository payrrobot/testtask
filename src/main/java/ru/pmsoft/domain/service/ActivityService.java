package ru.pmsoft.domain.service;

import ru.pmsoft.domain.dto.ActivityDto;

import java.util.List;

public interface ActivityService {

    ActivityDto saveActivity(ActivityDto activityDTO);

    void deleteActivity(Integer activityId);

    List<ActivityDto> findAll();
}
